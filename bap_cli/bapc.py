#!/usr/bin/env python3.6
import config
from commands import Moloch, wrBTC, Help, Account, rBTC
import sys

COMMANDS = [
            ### General helper functions about accounts
            {"args": ["help"], "func": Help.help,
             "help": Help.help_help},
            {"args": ["status"], "func": Account.status,
             "help": Account.help_status},
            {"args": ["account", "connect"], "func": Account.connect,
             "help": Account.help_connect},
            {"args": ["account", "changeProfile"], "func": Account.set_profile,
             "help": Account.help_set_profile},
            {"args": ["account", "showProfiles"], "func": Account.show_profiles,
             "help": Account.help_show_profiles},
            {"args": ["account", "setConfig"], "func": Account.set_config,
             "help": Account.help_set_config},
            ### Functions for basic rbtc exchanging.
            {"args": ["rbtc", "balanceOf"], "func": rBTC.balance_of,
             "help": rBTC.help_balance_of},
            {"args": ["rbtc", "send"], "func": rBTC.send_rbtc,
             "help": rBTC.help_send_rbtc},
            {"args": ["rbtc", "cashOut"], "func": rBTC.cash_out_rbtc,
             "help": rBTC.help_cash_out_rbtc},
            {"args": ["rbtc", "computeFromBTCKey"], "func": rBTC.convert_btc_to_rbtc_address,
             "help": rBTC.help_convert_btc_to_rbtc_address},
            {"args": ["rbtc", "getFederationAddress"], "func": rBTC.get_federation_address,
             "help": rBTC.help_get_federation_address},
            ### Functions for wrbtc exchanging.
            {"args": ["wrbtc", "deposit"], "func": wrBTC.deposit,
             "help": wrBTC.help_deposit},
            {"args": ['wrbtc', 'withdraw'], "func": wrBTC.withdraw,
             "help": wrBTC.help_withdraw},
            {"args": ['wrbtc', 'approve'], "func": wrBTC.approve,
             "help": wrBTC.help_approve},
            {"args": ['wrbtc', 'transfer'], "func": wrBTC.transfer,
             "help": wrBTC.help_transfer},
            {"args": ['wrbtc', 'transferFrom'], "func": wrBTC.transfer_from,
             "help": wrBTC.help_transfer_from},
            {"args": ['wrbtc', 'balanceOf'], "func": wrBTC.balance_of,
             "help": wrBTC.help_balance_of},
            {"args": ['wrbtc', 'totalSupply'], "func": wrBTC.total_supply,
             "help": wrBTC.help_total_supply},
            ### Moloch contract functions.
            #{"args": ['moloch', 'proposals'], "func": Moloch.proposals,
            # "help": Moloch.help_proposals},
            {"args": ['moloch', 'submitProposal'], "func": Moloch.submit_proposal,
             "help": Moloch.help_submit_proposal},
            {"args": ['moloch', 'submitVote'], "func": Moloch.submit_vote,
             "help": Moloch.help_submit_vote},
            {"args": ['moloch', 'processProposal'], "func": Moloch.process_proposal,
             "help": Moloch.help_process_proposal},
            {"args": ['moloch', 'ragequit'], "func": Moloch.ragequit,
             "help": Moloch.help_ragequit},
            {"args": ['moloch', 'abort'], "func": Moloch.abort,
             "help": Moloch.help_abort},
            {"args": ['moloch', 'updateDelegateKey'], "func": Moloch.update_delegate_key,
             "help": Moloch.help_update_delegate_key},
            {"args": ['moloch', 'getProposalQueueLength'], "func": Moloch.get_proposal_queue_length,
             "help": Moloch.help_get_proposal_queue_length},
            {"args": ['moloch', 'getProposal'], "func": Moloch.get_proposal,
             "help": Moloch.help_get_proposal},
            {"args": ['moloch', 'canRageQuit'], "func": Moloch.can_rage_quit,
             "help": Moloch.help_can_rage_quit},
            {"args": ['moloch', 'hasVotingPeriodExpired'], "func": Moloch.has_voting_period_expired,
             "help": Moloch.help_has_voting_period_expired},
            {"args": ['moloch', 'getMemberProposalVote'], "func": Moloch.get_member_proposal_vote,
             "help": Moloch.help_get_member_proposal_vote},
            {"args": ['moloch', 'getMembers'], "func": Moloch.get_members,
             "help": Moloch.help_get_members}
           ]

class BapCLI:
    @classmethod
    def show_help(cls, help_index=None, arg_start=None):
        if help_index is not None and COMMANDS[help_index].get('help'):
            COMMANDS[help_index]['help']()
        else:
            print("Usage: bapc.sh COMMAND argument1 argument2 ...")
            if arg_start and arg_start in ['account', 'moloch', 'rbtc', 'wrbtc']:
                print("\nPossible \""+arg_start+"\" commands:")
            else:
                print("\nPossible commands:")
            for com in COMMANDS:
                strr = " "
                for arg in com['args']:
                    strr = strr+" "+arg
                if arg_start and arg_start in ['account', 'moloch', 'rbtc', 'wrbtc'] and com['args'][0] != arg_start:
                    continue
                print(strr)
            print("\nTo see a command's help page, run bapc.sh help COMMAND")

    @classmethod
    def parse(cls, *args):
        kwargs = dict()
        args = list()
        key = None
        keys_started = False
        if len(sys.argv) == 1:
            cls.show_help()
            return

        for arg in sys.argv[1:]:
            if key is None and arg.startswith("--"):
                key = arg[2:]
                keys_started = True
            elif key is not None and arg.startswith("--"):
                kwargs[key] = 1
                key = arg[2:]
            elif key:
                kwargs[key] = arg
                key = None
            elif keys_started is False:
                args.append(arg)
            else:
                raise ARGException("Positional argument must be before keyword arguments: "+arg)
        if key:
            kwargs[key] = 1
        chelp = False
        if args and args[0].lower() == "help":
            chelp = True
            args = args[1:]
            if len(args) == 0:
                cls.show_help()
                return
        for j, entry in enumerate(COMMANDS):
            for i, arg in enumerate(entry['args']):
                if len(args) > i and args[i] == arg:
                    continue
                break
            else:
                #run this command
                if chelp:
                    cls.show_help(j)
                    break
                else:
                    try:
                        entry['func'](*args[len(entry['args']):], **kwargs)
                    except Exception as e:
                        #import traceback
                        #traceback.print_exc()
                        print(e)
                        print("")
                        cls.show_help(j)
                    break
        else:
            #no matching command found... show help.
            if args and args[0]:
                cls.show_help(arg_start=args[0])      

def main():
    config.load_config(config.get_profile())
    BapCLI.parse()

if __name__=='__main__':
    main()



    


