from web3 import Web3
import json
import time
import os
import os.path
import hashlib
from Crypto.Cipher import AES
from Crypto import Random
from getpass import getpass

w3 = None
network = "mainnet"#"regtest"#"regtest", "testnet", "mainnet"
http_url = ""
web_socket_url = ""

wrbtc_address = ""
moloch_address = ""
federation_address = ""

wrbtc_contract = None
moloch_contract = None
federation_contract = None

bap_server = "http://localhost:4545"
public_key = None
private_key = None
chain_id = 33
gas_limit = None

profile = "bapc"

UNITS_PER_SHARE = 10**10

HOME_DIR = os.getenv("HOME")
HOME_BAPD = os.path.join(HOME_DIR, ".bap")

if not os.path.exists(HOME_BAPD):
    os.mkdir(HOME_BAPD)

if not os.path.exists(os.path.join(HOME_BAPD, profile+".json")):
    ff = open(os.path.join(HOME_BAPD, profile+".json"), "w")
    ff.write(json.dumps({"private_key": "0"*64, "public_key": "0"*40, 
                         "network":"mainnet", "gas_limit": None}))
    ff.close()

if not os.path.exists(os.path.join(HOME_BAPD, "current_profile")):
    ff = open(os.path.join(HOME_BAPD, "current_profile"), "w")
    ff.write(profile)
    ff.close()

def encrypt_value(value, password):
    iv = Random.new().read(AES.block_size)
    key = hashlib.sha256(password.encode("utf-8")).digest()[:16]
    aes = AES.new(key, AES.MODE_CBC, iv)
    while len(value.encode('utf-8'))%AES.block_size != 0:
        value = value+" "
    value = value.encode('utf-8')
    encd = aes.encrypt(value)
    return [bytearray(iv).hex(), bytearray(encd).hex()]

def decrypt_value(e_value, password):
    iv = bytes(bytearray.fromhex(e_value[0]))
    key = hashlib.sha256(password.encode("utf-8")).digest()[:16]
    aes = AES.new(key, AES.MODE_CBC, iv)
    value = aes.decrypt(bytes(bytearray.fromhex(e_value[1])))
    value = value.decode("utf-8").strip()
    return value

def set_profile(profile_name):
    global profile
    ff = open(os.path.join(HOME_BAPD,"current_profile"), "w")
    ff.write(profile_name)

def get_profile():
    data = open(os.path.join(HOME_BAPD,"current_profile")).read()
    return data

def get_profiles():
    output = list()
    for filename in os.listdir(HOME_BAPD):
       if filename.endswith(".json"):
            try:
                dd = json.loads(open(os.path.join(HOME_BAPD,filename)).read())
                output.append(filename.partition(".json")[0])
            except:
                pass
    return output

def save_config(arg, value, password=""):
    profile = get_profile()
    try:
        data = json.loads(open(os.path.join(HOME_BAPD,profile+".json")).read())
    except:
        data = dict()
    if password:
        data[arg] = encrypt_value(value, password)
    else:
        data[arg] = value
    ff = open(os.path.join(HOME_BAPD, profile+".json"), "w")
    ff.write(json.dumps(data))

def load_config(config=None):
    global network
    global http_url
    global web_socket_url
    global wrbtc_address
    global moloch_address
    global federation_address
    global bap_server
    global w3
    global wrbtc_contract
    global moloch_contract
    global federation_contract
    global public_key
    global private_key
    global chain_id
    global gas_limit
    global profile
    print("Using profile: "+get_profile())
    if config:
        profile = config
        try:
            data = json.loads(open(os.path.join(HOME_BAPD,config+".json")).read())
        except:
            data = dict()
    else:
        data = dict()
    network = data.get("network", "regtest")

    if network == "regtest":
        web_socket_url = "ws://127.0.0.1:4445/websocket"
    elif network == "testnet":
        http_url = "https://public-node.testnet.rsk.co"
    elif network == "mainnet": 
        http_url = "https://public-node.rsk.co"

    if (web_socket_url):
        w3 = Web3(Web3.WebsocketProvider(web_socket_url))
    else:
        w3 = Web3(Web3.HTTPProvider(http_url))

    if network == "regtest": 
        wrbtc_address = "0x83C5541A6c8D2dBAD642f385d8d06Ca9B6C731ee"
        moloch_address = "0x8901a2Bbf639bFD21A97004BA4D7aE2BD00B8DA8"
        federation_address = "0x0000000000000000000000000000000001000006"
        chain_id = 33
    elif network == "testnet":
        wrbtc_address = "0xaDef1374554324d0311547261fDF052598c8b42a"
        moloch_address = "0x657ABFa8EF2175045Fe7a87935Bf6E3CAb9be9fE"
        federation_address = "0x0000000000000000000000000000000001000006"
        chain_id = 31
    elif network == "mainnet":
        wrbtc_address = "0x1945d8aEdc005C900C796E31cce040363A6658d2"
        moloch_address = "0x240404667dDe830EA7D0bc6FA9321383691De60B"
        federation_address = "0x0000000000000000000000000000000001000006"
        chain_id = 30

    wrbtc_abi = json.loads(open("contract_abis/WRBTC9.json").read())
    wrbtc_abi = json.dumps(wrbtc_abi['abi'])
    wrbtc_contract = w3.eth.contract(wrbtc_address, abi=wrbtc_abi)

    moloch_abi = json.loads(open("contract_abis/Moloch.json").read())
    moloch_abi = json.dumps(moloch_abi['abi'])
    moloch_contract = w3.eth.contract(moloch_address, abi=moloch_abi)

    try:
        federation_abi = json.loads(open("contract_abis/RSKFederationAddress.json").read())
        federation_abi = json.dumps(federation_abi['abi'])
        federation_contract = w3.eth.contract(federation_address, abi=federation_abi)
    except:
        federation_contract = None

    """
    try:
        private_key_hex = data.get("private_key", "0"*64)#"c85ef7d79691fe79573b1a7064c19c1a9819ebdbd1faaab1a8ec92344438aaf4")
        private_key = bytearray.fromhex(private_key_hex)
    except:
        private_key = bytearray.fromhex("0"*64)
    """
    private_key = data.get("private_key", "0"*64)

    public_key_hex = data.get("public_key", "0"*40)#"cd2a3d9f938e13cd947ec05abc7fe734df8dd826")
    try:
        public_key = w3.toChecksumAddress(bytearray.fromhex(public_key_hex))
    except:
        public_key = w3.toChecksumAddress(bytearray.fromhex("0"*40))

    gas_limit = data.get("gas_limit", None)
    
    import contracts
    contracts.wrBTCContract._contract = wrbtc_contract
    contracts.MolochContract._contract = moloch_contract
    contracts.FederationContract._contract = federation_contract

def load_private_key():
    global private_key
    if type(private_key) in [tuple, list]:
        #private key is encrypted
        password = getpass("Password: ")
        res = decrypt_value(private_key, password)
        res = bytearray.fromhex(res)
        private_key = res
    elif len(private_key) == 64:
        private_key = bytearray.fromhex(private_key)
    
