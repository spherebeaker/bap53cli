from web3 import Web3
import config
import json
import time

class EtherUtil:
    @classmethod
    def send_ether(cls, public_key, private_key, to, amount, units="ether",
                   gas_price=None, estimate_gas=None):
        public_key = config.w3.toChecksumAddress(public_key)
        nonce = config.w3.eth.getTransactionCount(public_key)
        txn = {"from": public_key, 
               "to": to,
               "value": config.w3.toWei(amount, units),
               "nonce": nonce,
               #"chainId": config.chain_id
              }
        if estimate_gas is None:
            try:
                estimate_gas = config.w3.eth.estimateGas(txn)
            except:
                estimate_gas = config.w3.eth.getBlock("latest").gasLimit
            print("Estimated gas:", estimate_gas)
        if gas_price is None:
            gas_price = config.w3.eth.gasPrice
        txn['gas'] = estimate_gas
        txn['gasPrice'] = gas_price
        signed = config.w3.eth.account.sign_transaction(txn, private_key)
        return config.w3.eth.sendRawTransaction(signed.rawTransaction)

    @classmethod
    def is_transaction_pushed(cls, txn_hash):
        res = config.w3.eth.getTransaction(txn_hash)
        if res.blockHash is not None:
            return True
        return False

    @classmethod
    def wait_for_transaction(cls, txn_hash):
        while True:
            try:
                if cls.is_transaction_pushed(txn_hash):
                    return_val = config.w3.eth.getTransactionReceipt(txn_hash)
                    if return_val['status'] == 0:
                        print("transaction ran out of gas?")
                    return return_val
            except Exception as e:
                print("Exception... Block number:",config.w3.eth.getBlock("latest").number)
                print(e)
                print("Trying to continue...")
                raise
            time.sleep(5)

    @classmethod
    def getBlockNumber(cls):
        return config.w3.eth.blockNumber


class SmartContract(object):
    @classmethod
    def write_transaction(cls, public_key, private_key, function_name, *args):
        public_key = config.w3.toChecksumAddress(public_key)
        gas_estimate = getattr(cls._contract.functions, function_name)(*args).estimateGas();
        gas_price = config.w3.eth.gasPrice
        gas_limit = config.w3.eth.getBlock("latest").gasLimit
        if True:#contract calls always seem to use more gas.
            gas_estimate = gas_limit
        if config.gas_limit and config.gas_limit  <  gas_estimate:
            gas_estimate = config.gas_limit
        nonce = config.w3.eth.getTransactionCount(public_key)
        txn = getattr(cls._contract.functions, function_name)(*args).buildTransaction({"chainId": config.chain_id, 
            "gas": gas_estimate, "gasPrice": gas_price, "nonce": nonce})
        signed_txn = config.w3.eth.account.sign_transaction(txn, private_key=private_key)
        #print(txn)
        #print(signed_txn)
        return config.w3.eth.sendRawTransaction(signed_txn.rawTransaction)

    @classmethod
    def call_contract(cls, function_name, *args):
        return getattr(cls._contract.functions, function_name)(*args).call()

    @classmethod
    def get_variable(cls, key, *args):
        value = getattr(cls._contract.functions, key)(*args).call()
        return value


class wrBTCContract(SmartContract):
    _contract = None#config.wrbtc_contract

    @classmethod
    def deposit(cls, public_key, private_key, amount, units="ether"):
        return EtherUtil.send_ether(public_key, private_key, config.wrbtc_address, amount, units="ether")

    @classmethod
    def withdraw(cls, public_key, private_key, amount, units="ether"):
        value = config.w3.toWei(amount, units)
        return cls.write_transaction(public_key, private_key, "withdraw", value)

    @classmethod
    def approve(cls, public_key, private_key, address, wad, units="ether"):
        address = config.w3.toChecksumAddress(address)
        wad = config.w3.toWei(wad, units)
        return cls.write_transaction(public_key, private_key, "approve", address, wad)

    @classmethod
    def transfer(cls, public_key, private_key, address, wad, units="ether"):
        address = config.w3.toChecksumAddress(address)
        wad = config.w3.toWei(wad, units)
        return cls.write_transaction(public_key, private_key, "transfer", address, wad)

    @classmethod
    def transferFrom(cls, public_key, private_key, source_address, target_address, wad, units="ether"):
        address = config.w3.toChecksumAddress(address)
        wad = config.w3.toWei(wad, units)
        return cls.write_transaction(public_key, private_key, "transferFrom", source_address, target_address, wad)

    @classmethod
    def totalSupply(cls):
        return cls.call_contract("totalSupply")

    @classmethod
    def balanceOf(cls, public_key):
        public_key = config.w3.toChecksumAddress(public_key)
        return cls.call_contract("balanceOf", public_key)


class MolochContract(SmartContract):
    _contract = None#config.moloch_contract

    @classmethod
    def submitProposal(cls, public_key, private_key, applicant, token_tribute, shares_requested, details, units="ether"):
        public_key = config.w3.toChecksumAddress(public_key)
        applicant = config.w3.toChecksumAddress(applicant)
        token_tribute = config.w3.toWei(token_tribute, units)
        
        approval_amount = cls.get_variable("proposalDeposit")+token_tribute
        #Approve token transfer and proposal deposity
        print("Approving for "+str(config.w3.fromWei(approval_amount, "ether"))+" ether.  tokenTribute = "+str(config.w3.fromWei(token_tribute, "ether"))+" ether, proposalDeposit = "+str(config.w3.fromWei(cls.get_variable("proposalDeposit"), "ether"))+" ether")
        ap_txn = wrBTCContract.approve(public_key, private_key, cls._contract.address, approval_amount*1, units=units)

        print("Waiting for approval...")
        EtherUtil.wait_for_transaction(ap_txn)
        print("Submitting proposal...")
        return cls.write_transaction(public_key, private_key, "submitProposal", applicant, token_tribute, shares_requested, details)

    @classmethod
    def submitVote(cls, public_key, private_key, proposal_index, vote):
        public_key = config.w3.toChecksumAddress(public_key)
        vote_dict = {"yes": 1, "no": 2, "null": 0, None: 0}
        vote = vote_dict.get(vote, vote)
        return cls.write_transaction(public_key, private_key, "submitVote", proposal_index, vote)

    @classmethod
    def processProposal(cls, public_key, private_key, proposal_index):
        public_key = config.w3.toChecksumAddress(public_key)
        return cls.write_transaction(public_key, private_key, "processProposal", proposal_index)

    @classmethod
    def ragequit(cls, public_key, private_key, shares_to_burn):
        public_key = config.w3.toChecksumAddress(public_key)
        return cls.write_transaction(public_key, private_key, "ragequit", shares_to_burn)

    @classmethod
    def abort(cls, public_key, private_key, proposal_index):
        public_key = config.w3.toChecksumAddress(public_key)
        return cls.write_transaction(public_key, private_key, "abort", proposal_index)

    @classmethod
    def updateDelegateKey(cls, public_key, private_key, new_delegate_key):
        public_key = config.w3.toChecksumAddress(public_key)
        return cls.write_transaction(public_key, private_key, "updateDelegateKey", new_delegate_key)

    ## Getters

    @classmethod
    def getCurrentPeriod(cls):
        return cls.call_contract("getCurrentPeriod")

    @classmethod
    def getProposalQueueLength(cls):
        return cls.call_contract("getProposalQueueLength")

    @classmethod
    def getProposal(cls, proposal_index):
        return cls.call_contract("getProposal", proposal_index)  
 
    @classmethod
    def canRageQuit(cls, highest_index_yes_vote):
        return cls.call_contract("canRageQuit", highest_index_yes_vote)

    @classmethod
    def hasVotingPeriodExpired(cls, starting_period):
        return cls.call_contract("hasVotingPeriodExpired", starting_period)

    @classmethod
    def getMemberProposalVote(cls, member_address, proposal_index):
        member_address = config.w3.toChecksumAddress(member_adress)
        return cls.call_contract("getMemberProposalVote", member_address, proposal_index)
      

class FederationContract(SmartContract):
    _contract = None#config.federation_contract

    @classmethod
    def getFederationAddress(cls):
        return cls.call_contract("getFederationAddress")


def main2():
    pass   

def main():
    """
    print(EtherUtil.getBlockNumber(), "wrBTC::deposit")
    txn = wrBTCContract.deposit(config.public_key, config.private_key, 2.01, units="ether")
    EtherUtil.wait_for_transaction(txn)
    print(config.w3.eth.blockNumber, wrBTCContract.balance_of(config.public_key))
    
    print(EtherUtil.getBlockNumber(), "wrBTC::withdraw")
    txn_hash = wrBTCContract.withdraw(config.public_key, config.private_key, 1, units="wei")
    
    print(txn_hash.hex())
    vv = EtherUtil.wait_for_transaction(txn_hash)
    """
    print(config.w3.eth.blockNumber, wrBTCContract.balance_of(config.public_key))

    print(EtherUtil.getBlockNumber(), "Moloch::submitProposal")
    config.load_private_key()
    txn_hash = MolochContract.submit_proposal(config.public_key, config.private_key, config.public_key, config.w3.toWei(1, "ether"), 1000000, "example", units="wei")

    EtherUtil.wait_for_transaction(txn_hash)
    tx_receipt = config.w3.eth.getTransactionReceipt(txn_hash)
    print(tx_receipt)
    import pdb
    pdb.set_trace()
    print(MolochContract.get_proposal_queue_length())
    print("DONE")
    import pdb
    pdb.set_trace()
    print("AAA")



def main2():
    config.load_config("regtest")
    doc = json.loads(open("contract_abis/WRBTC9.json").read())
    bytecode = doc['bytecode']
    abi = doc['abi']
    contract = config.w3.eth.contract(abi=abi, bytecode=bytecode)
    import pdb
    pdb.set_trace()
    tx_hash = contract.constructor().transact()
    tx_receipt = config.w3.eth.waitForTransactoinReceipt(tx_hash)
    contract_address = tx_receipt.contractAddress


if __name__=='__main__':
    main2()
