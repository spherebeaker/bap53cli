from exceptions import ARGException
from contracts import wrBTCContract, MolochContract, EtherUtil,\
                      FederationContract
import config 
import json
import hashlib
import base58
import time
import http.client
from getpass import getpass

class Help:
    @classmethod
    def help(cls):
        pass

    @classmethod
    def help_help(cls):
        print("Shows description for given command.")

class Account:
    @classmethod
    def status(cls, *args, **kwargs):
        public_key = config.public_key
        if len(args) > 0 and args[0] == "show":
            config.load_private_key()
            show_private_key = config.private_key.hex()
        else:
            show_private_key = "<use 'status show' to show private key>"
       
        try:
            rbtc_balance = config.w3.fromWei(config.w3.eth.getBalance(config.public_key), "ether")
            rbtc_balance = str(rbtc_balance)+" ether"
        except:
            rbtc_balance = "Not connected."
       
        try:
            wrbtc_balance = str(config.w3.fromWei(wrBTCContract.balanceOf(config.public_key), "ether"))+" ether"
        except:
            if int(config.public_key, 16) == 0:
                wrbtc_balance = "Invalid public key"
                public_key = "Invalid public key"
            else:
                wrbtc_balance = "Not connected"

        try:
            moloch_member = MolochContract.get_variable("members", config.public_key)
            moloch_bank = str(config.w3.fromWei(wrBTCContract.balanceOf(MolochContract._contract.address), "ether"))+" ether"
            moloch_shares = moloch_member[1]/config.UNITS_PER_SHARE
     
            moloch_shares_total = MolochContract.get_variable("totalShares")/config.UNITS_PER_SHARE
        except:
            moloch_member = "Not connected"
            moloch_bank = "Not connected"
            moloch_shares = "Not connected"
            moloch_shares_total = "Not connected"

        try:
            gas_limit_network = config.w3.eth.getBlock("latest").gasLimit
        except:
            gas_limit_network = "Not connected"

        url = config.web_socket_url
        if not url:
            url = config.http_url

        data = {"public_key": public_key,
                "private_key": show_private_key,
                "connection": url,
                "chain_id": config.chain_id,
                "wrbtc_address": config.wrbtc_address,
                "wrbtc_balance": wrbtc_balance,
                "network": config.network,
                "moloch_address": config.moloch_address,
                "moloch_shares": moloch_shares,
                "moloch_shares_total": moloch_shares_total,
                "moloch_bank": moloch_bank,
                "gas_limit_config": config.gas_limit,
                "gas_limit_network": gas_limit_network,
                "rbtc_balance": rbtc_balance
               }

        print("Current profile: "+config.profile)
        print(json.dumps(data, sort_keys=True, indent=4))
      
    @classmethod
    def help_status(cls):
        print("Shows the current configuration of bapc, and basic state of the smart contracts.")
        print("Example:\n bapc.sh status")

    @classmethod
    def connect(cls, *args, **kwargs):
        if len(args) < 1:
            raise ARGException("Not enough arguments.")
  
        network = args[0]
        if network not in ['regtest', 'testnet', 'mainnet']:
            raise ARGException("Network must be one of 'regtest', 'testnet', 'or 'mainnet'.")
        config.save_config("network", network)
        print("Network id now "+network)

    @classmethod
    def help_connect(cls):
        print("Connects the current profile to the given network.")
        print("Example:\n bapc.sh account connect testnet")

    @classmethod
    def set_profile(cls, *args, **kwargs):
        if len(args) < 1:
            raise ARGException("Not enough arguments.")
        profile = args[0]
        config.set_profile(profile)
        print("Changed profile to '"+profile+"'")

    @classmethod
    def help_set_profile(cls):
        print("Changes the current profile.")
        print("Example:\n bapc.sh changeProfile test_profile")

    @classmethod
    def show_profiles(cls, *args, **kwargs):
        profiles = config.get_profiles()
        current_profile = config.get_profile()
        print("Existing profiles:")
        for profile in profiles:
            if profile == current_profile:
                print(">>"+profile)
            else:
                print("  "+profile)

    @classmethod
    def help_show_profiles(cls):
        print("Shows existing profiles.")
        print("Example:\n bapc.sh showProfiles")

    @classmethod
    def set_config(cls, *args, **kwargs):
        if len(args) < 1:
            raise ARGException("Not enough arguments.")
        key = args[0]
        keys = ["network", "private_key", "public_key", "gas_limit"]
        encrypt = False
        password = ""
        if key not in keys:
            raise ARGException("Invalid configuration key.  Must be one of: "+", ".join(keys[:-1])+" or "+keys[-1]+".")

        value = input("Enter value for "+key+": ")

        if key == "gas_limit":
            if value.lower() == "none":
                value = None
            else:
                value = int(value)

        if key in ["private_key", "public_key"]:
            if value.startswith("0x"):
                value = value[2:]
            if key == "private_key" and len(value) != 64:
                raise ARGException("Private key must be 64 hex characters long.")
            if key == "public_key" and len(value) != 40:
                raise ARGException("Public key must be 40 hex characters long.")
            if key == "private_key":
                #Ask for private key password...
                while True:
                    password = getpass("Password: ")
                    confirm_pass = getpass("Confirm password: ")
                    if password != confirm_pass:
                        print("Passwords don't match... Try again.")
                        
                    elif len(password) < 12 and password:
                        while True:
                            value4 = input("Password is less than 12 characters, are you sure? y/n:")
                            if value4.lower().strip() in ['y', 'yes']:
                                value4 = True
                                break
                            elif value4.lower().strip() in ['n', 'no']:
                                value4 = False
                                break
                        if value4:
                            break
                    else:
                        break
        config.save_config(key, value, password)
        print("Config updated.")        

    @classmethod
    def help_set_config(cls):
        print("Changes the configuration option for the current profile.  Configuration options are 'network', 'private_key', 'public_key', and 'gas_limit'.")
        print("Example:\n bapc.sh setConfig gas_limit")
        print(" Enter value for gas_limit: none")


class wrBTC:
    @classmethod
    def deposit(cls, *args, **kwargs):
        if len(args) < 2:
            raise ARGException("Not enough arguments")
        amount = args[0]
        units = args[1]
        if float(amount) > 0:
            try:
                value = config.w3.toWei(amount, units)
            except:
                raise ARGException("Invalid unit type.  Must be 'ether', 'wei', gwei', etc.")
        else:
            raise ARGException("amount to deposit must be > 0")
        config.load_private_key()     
        res = wrBTCContract.deposit(config.public_key, config.private_key, amount, units=units)
        res = EtherUtil.wait_for_transaction(res)

        if res['status'] == 1:
            print("Deposit successful.")

    @classmethod
    def help_deposit(cls):
        print("Converts rBTC into usable wrBTC (wrapped rBTC) that can be used in the Moloch contract.  Use withdraw to convert back to rBTC.")
        print("Example:\n bapc.sh wrbtc deposit 0.5 ether")

    @classmethod
    def withdraw(cls, *args, **kwargs):
        if len(args) < 2:
            raise ARGException("Not enough arguments")
        amount = args[0]
        units = args[1]
        if float(amount) > 0:
            try:
                value = config.w3.toWei(amount, units)
            except:
                raise ARGException("Invalid unit type.  Must be 'ether', 'wei', gwei', etc.")
        else:
            raise ARGException("amount to withdraw must be > 0")
        config.load_private_key()
        res = wrBTCContract.withdraw(config.public_key, config.private_key, amount, units)    
        res = EtherUtil.wait_for_transaction(res)   
        if res['status'] == 1:
            print("Withdraw successful.")

    @classmethod
    def help_withdraw(cls):
        print("Converts wrBTC (wrapped rBTC) into rBTC.  rBTC can be used for gas, and can be converted into BTC, but can not be used with the Moloch contract directly.  Use deposit to convert rBTC to wrBTC for use in Moloch.")
        print("Example:\n bapc.sh wrbtc withdraw 0.1 ether")

    @classmethod
    def approve(cls, *args, **kwargs):
        if len(args) < 3:
            raise ARGException("Not enough arguments")
        address = config.w3.toChecksumAddress(args[0])
        amount = args[1]
        units = args[2]

        if float(amount) > 0:
            try:
                value = config.w3.toWei(amount, units)
            except:
                raise ARGException("Invalid unit type.  Must be 'ether', 'wei', gwei', etc.")
        else:
            raise ARGException("Amount to approve must be > 0")
        config.load_private_key()
        res = wrBTCContract.approve(config.public_key, config.private_key, address, amount, units)        
        res = EtherUtil.wait_for_transaction(res)   
        if res['status'] == 1:
            print("Approve successful.")

    @classmethod
    def help_approve(cls):
        print("Approves the destination address to spend the user's wrBTC.  This is needed to allow a contract to accept wrBTC from the user.  This is done automatically when submitting proposals to the Moloch contract with bapc.")
        print("Example:\n bapc.sh wrbtc approve 0x8901a2Bbf639bFD21A97004BA4D7aE2BD00B8DA8 0.01 ether")

    @classmethod
    def transfer(cls, *args, **kwargs):
        if len(args) < 3:
            raise ARGException("Not enough arguments")
        address = config.w3.toChecksumAddress(args[0])
        amount = args[1]
        units = args[2]

        if float(amount) > 0:
            try:
                value = config.w3.toWei(amount, units)
            except:
                raise ARGException("Invalid unit type.  Must be 'ether', 'wei', gwei', etc.")
        else:
            raise ARGException("Amount to transfer must be > 0")
 
        config.load_private_key()
        res = wrBTCContract.transfer(config.public_key, config.private_key, address, amount, units)        
        res = EtherUtil.wait_for_transaction(res)   
        if res['status'] == 1:
            print("Transfer successful.")

    @classmethod
    def help_transfer(cls):
        print("Transfers the amount of wrBTC to the destination address.")
        print("Example:\n bapc.sh wrbtc transfer 0x8901a2Bbf639bFD21A97004BA4D7aE2BD00B8DA8 0.05 ether")

    @classmethod
    def transfer_from(cls, *args, **kwargs):
        if len(args) < 4:
            raise ARGException("Not enough arguments")
        address = config.w3.toChecksumAddress(args[0])
        target = config.w3.toChecksumAddress(args[1])
        amount = args[2]
        units = args[3]

        if float(amount) > 0:
            try:
                value = config.w3.toWei(amount, units)
            except:
                raise ARGException("Invalid unit type.  Must be 'ether', 'wei', gwei', etc.")
        else:
            raise ARGException("Amount to transfer from must be > 0")
        config.load_private_key()
        res = wrBTCContract.transferFrom(config.public_key, config.private_key, address, target, amount, units)
        res = EtherUtil.wait_for_transaction(res)   
        if res['status'] == 1:
            print("TransferFrom successful.")

    @classmethod
    def help_transfer_from(cls):
        print("Transfers the amount of wrBTC from the source address to the target address.  User's address must be approved to send wrBTC from source address.")
        print("Example:\n bapc.sh wrbtc transferFrom <source_address> <target address> 0.05 ether")

    @classmethod
    def total_supply(cls, *args, **kwargs):
        res = wrBTCContract.totalSupply()   
        print(res, "wei")
        print(config.w3.fromWei(res, "ether"), "ether")

    @classmethod
    def help_total_supply(cls):
        print("Returns the total supply of wrBTC in circulation.")
        print("Example:\n bapc.sh wrbtc totalSupply")

    @classmethod
    def balance_of(cls, *args, **kwargs):
        if len(args) > 0:
            address = config.w3.toChecksumAddress(args[0])
        else:
            address = config.public_key
 
        res = wrBTCContract.balanceOf(address)  
        print(res, "wei")
        print(config.w3.fromWei(res, "ether"), "ether")

    @classmethod
    def help_balance_of(cls):
        print("Gets the balance of wrBTC of the given address.  Defauts to user's address.")
        print("Example:\n bapc.sh wrbtc balanceOf 0x8901a2Bbf639bFD21A97004BA4D7aE2BD00B8DA8")

 
class Moloch:
    @classmethod
    def _submit_details_to_website(cls, details, details_hash, proposal_num):
        if hashlib.sha256(details).hexdigest() == details_hash:
            doc = {"hash": details_hash, "details": details.decode("utf-8"), 
                   "address": config.public_key, "proposal_num": proposal_num}
            #print(doc)
            body = json.dumps(doc)
            conn = http.client.HTTPSConnection("api.bap53.com", 443)
            conn.request("POST", "/details_hash", body)
            response = conn.getresponse()
            if response.status == 200:
                pass
            else:
                raise Exception("error: saving hash failed.")
        else:
            raise Exception("error: hashs don't match")
    @classmethod
    def _get_proposal_details_from_hash(cls, details_hash, num=None):
        conn = http.client.HTTPSConnection("api.bap53.com", 443)
        conn.request("GET", "/details_hash?hash="+details_hash)
        response = conn.getresponse()
        body = json.loads(response.read())
        return body['details']

    @classmethod
    def submit_proposal(cls, *args, **kwargs):
        if len(args) < 5:
            raise ARGException("Not enough arguments")
       
        applicant = args[0]
        token_tribute = float(args[1])
        units = args[2]
        shares_requested = int(float(args[3])*config.UNITS_PER_SHARE)
        details = open(args[4]).read().encode("utf-8")
        details_hash = hashlib.sha256(details).hexdigest()
 
        proposal_num = cls._get_proposal_queue_length()
        cls._submit_details_to_website(details, details_hash, proposal_num)
        #submit proposal to website.
        #also do a check to see if the  
        config.load_private_key()
        res = MolochContract.submitProposal(config.public_key, config.private_key, applicant, token_tribute, shares_requested, details_hash, units)  
        res = EtherUtil.wait_for_transaction(res)
        if res['status'] == 1:
            print("SubmitProposal successful.")

    @classmethod
    def help_submit_proposal(cls):
        print("Submits the given proposal.  This can add a new member to the DAO, submit a tribute for shares, or grant shares to a member.")
        print("Command:\n bapc.sh moloch submitProposal applicant_address token_tribute units shares_requested details_file")
        print("Example:\n bapc.sh moloch submitProposal 0x83C5541A6c8D2dBAD642f385d8d06Ca9B6C731ee 1 ether 1000000 proposal1.txt")

    @classmethod
    def submit_vote(cls, *args, **kwargs):
        if len(args) < 2:
            raise ARGException("Not enough arguments")
       
        proposal_index = int(args[0])
        vote = args[1].lower()
        try:
            vote = int(vote)
        except:
            pass
        config.load_private_key()
        res = MolochContract.submitVote(config.public_key, config.private_key, proposal_index, vote)  
        res = EtherUtil.wait_for_transaction(res)   
        if res['status'] == 1:
            print("SubmitVote successful.")

    @classmethod
    def help_submit_vote(cls):
        print("Submits your vote on the given proposal.  Votes can be 'yes', 'no', or 'null'")
        print("Example:\n bapc.sh moloch submitVote 15 yes")

    @classmethod
    def process_proposal(cls, *args, **kwargs):
        if len(args) < 1:
            raise ARGException("Not enough arguments")
       
        proposal_index = int(args[0])
        config.load_private_key()
        res = MolochContract.processProposal(config.public_key, config.private_key, proposal_index)  
        res = EtherUtil.wait_for_transaction(res)   
        if res['status'] == 1:
            print("ProcessProposal successful.")

    @classmethod
    def help_process_proposal(cls):
        print("Processes the given proposal.  If the proposal requires processing, then the processing reward will be given to the current user.")
        print("Example:\n bapc.sh moloch processProposal 15")

    @classmethod
    def ragequit(cls, *args, **kwargs):
        if len(args) < 1:
            raise ARGException("Not enough arguments")
       
        shares_to_burn = int(float(args[0])*config.UNITS_PER_SHARE)
        config.load_private_key()
        res = MolochContract.ragequit(config.public_key, config.private_key, shares_to_burn)  
        res = EtherUtil.wait_for_transaction(res)   
        if res['status'] == 1:
            print("Ragequit successful.")

    @classmethod
    def help_ragequit(cls):
        print("Makes the current user ragequit from the DAO.  Will burn the number of shares given and withdraw the proportional number of wrBTC tokens.")
        print("Example:\n bapc.sh moloch ragequit 15000000")

    @classmethod
    def abort(cls, *args, **kwargs):
        if len(args) < 1:
            raise ARGException("Not enough arguments")
       
        proposal_index = int(args[0])
        config.load_private_key()
        res = MolochContract.abort(config.public_key, config.private_key, proposal_index)  
        res = EtherUtil.wait_for_transaction(res)   
        if res['status'] == 1:
            print("Abort successful.")

    @classmethod
    def help_abort(cls):
        print("Aborts the given processing proposal.")
        print("Example:\n bapc.sh moloch abort 0")

    @classmethod
    def update_delegate_key(cls, *args, **kwargs):
        if len(args) < 1:
            raise ARGException("Not enough arguments")
        
        new_delegate_key = args[0]
        config.load_private_key()
        res = MolochContract.updateDelegateKey(config.public_key, config.private_key, new_delegate_key)  
        res = EtherUtil.wait_for_transaction(res)   
        if res['status'] == 1:
            print("Update delegate key successful.")

    @classmethod
    def help_update_delegate_key(cls):
        print("Updates the user's delegate key.")
        print("Example:\n bapc.sh moloch updateDelegateKey 0x83C5541A6c8D2dBAD642f385d8d06Ca9B6C731ee")

    @classmethod
    def get_current_period(cls, *args, **kwargs):
        config.load_private_key()
        res = MolochContract.getCurrentPeriod(config.public_key, config.private_key)  
        print(res)

    @classmethod
    def help_get_current_period(cls):
        print("Returns the current period.")
        print("Example:\n bapc.sh moloch getCurrentPeriod")

    @classmethod
    def _get_proposal_queue_length(cls, *args, **kwargs):
        res = MolochContract.getProposalQueueLength()  
        return res 

    @classmethod
    def get_proposal_queue_length(cls, *args, **kwargs):
        res = cls._get_proposal_queue_length(*args, **kwargs)
        print(res)

    @classmethod
    def help_get_proposal_queue_length(cls):
        print("Returns the length of the proposal queue.")
        print("Example:\n bapc.sh moloch getProposalQueueLength")

    @classmethod
    def get_proposal(cls, *args, **kwargs):
        if len(args) < 1:
            raise ARGException("Not enough arguments")
        proposal_index = int(args[0])
        max_proposals = cls._get_proposal_queue_length()
        if proposal_index >= max_proposals:
            raise ARGException("Proposal number is higher than number of proposals.")
        elif proposal_index < 0:
            raise ARGException("Proposal number must be greater than or equal to 0.")
        res_org = MolochContract.get_variable("proposalQueue", proposal_index)  
        res = res_org[:]
        #res[-1] = fetch_hash
        res[2] = "{:,}".format(res[2])
        res[9] = str(config.w3.fromWei(res[9], "ether"))+" wrBTC"
        keys= ['Proposer', 'Applicant', 'Shares Requested', "Starting Period", "Yes Votes", "No Votes", "Processed", "Passed", "Aborted", "Token Tribute"]
 
        for i, key in enumerate(keys):
            print(keys[i]+":", res[i]) 
        print("Shares per wrBTC:", "{:,}".format(int(res_org[2]/config.w3.fromWei(res_org[9], "ether"))))
        print("Details:\n")
        details = cls._get_proposal_details_from_hash(res[10], proposal_index)
        print(details)

    @classmethod
    def help_get_proposal(cls):
        print("Returns the given proposal.  Proposals start at index 0.")
        print("Example:\n bapc.sh moloch getProposal 12")
        print(" bapc.sh moloch getProposal 0")



    @classmethod
    def can_rage_quit(cls, *args, **kwargs):
        highest_index_yes_vote = 0
        #TODO: make this variable to be the last proposal that the
        #      current user has voted yes on.
        config.load_private_key()
        res = MolochContract.canRageQuit(config.public_key, config.private_key, highest_index_yes_vote)  
        print(res)

    @classmethod
    def help_can_rage_quit(cls):
        print("Returns if the current user can ragequit based in the highest index yes vote.")
        print("Example:\n bapc.sh moloch canRageQuit")

    @classmethod
    def has_voting_period_expired(cls, *args, **kwargs):
        if len(args) < 1:
            raise ARGException("Not enough arguments")
        start_index = int(args[0])

        res = MolochContract.hasVotingPeriodExpired(start_index)  
        print(res)

    @classmethod
    def help_has_voting_period_expired(cls):
        print("Returns if the voting period for the given starting period has expired.")
        print("Example:\n bapc.sh moloch hasVotingPeriodExpired 1240")

    @classmethod
    def get_member_proposal_vote(cls, *args, **kwargs):
        if len(args) < 2:
            raise ARGException("Not enough arguments")
        member_address = args[0]
        proposal_index = int(args[1])
        config.load_private_key()
        res = MolochContract.getMemberProposalVote(config.public_key, config.private_key, member_address, proposal_index)  
        print(res)

    @classmethod
    def help_get_member_proposal_vote(cls):
        print("Returns the vote of this member for the given proposal.")
        print("Example:\n bapc.sh moloch get member proposal vote 12")

    @classmethod
    def get_members(cls):
        #members = {MolochContract.get_variable("summoner"):1}
        members = dict()
        try:
            summoner = MolochContract.get_variable("proposalQueue", 0)[0]
            members[summoner] = 1
        except:
            pass
        proposal_queue_length = cls._get_proposal_queue_length()
        for i in range(proposal_queue_length):
            res = MolochContract.get_variable("proposalQueue", i)
            if res[7] == True:
                members[res[1]] = 1
        
        for res in members.keys():
            print(res)

    @classmethod
    def help_get_members(cls):
        print("Returns the current members of this contract..")
        print("Example:\n bapc.sh moloch getMembers")


class rBTC:
    @classmethod
    def get_federation_address(cls):
        address = FederationContract.getFederationAddress()
        print("Network:", config.network)
        print("Address:", address)
        return address
  
    @classmethod
    def help_get_federation_address(cls):
        print("Returns the current federation address for the RSK network.")
        print("Example:\n bapc.sh rbtc getFederationAddress")

    @classmethod
    def balance_of(cls, *args):
        if len(args) > 0:
            address = args[0]
        else:
            address = config.public_key
        print("Address:", address)
        value = config.w3.fromWei(config.w3.eth.getBalance(address), "ether")
        print("Balance:", value)
        return value

    @classmethod
    def help_balance_of(cls):
        print("Returns the current rbtc balance for the given address.  Defaults to the configured public key.")
        print("Example:\n bapc.sh rbtc balanceOf 0x50D7b0aDfd593B8972d6bDcE974E43d37653e431")

    @classmethod
    def send_rbtc(cls, *args, **kwargs):
        if len(args) < 3:
            raise ARGException("Not enough arguments.")
 
        to = config.w3.toChecksumAddress(args[0])
        amount = args[1]
        units = args[2]

        if float(amount) > 0:
            try:
                value = config.w3.toWei(amount, units)
            except:
                raise ARGException("Invalid unit type.  Must be 'ether', 'wei', gwei', etc.")
        else:
            raise ARGException("Amount to transfer from must be > 0")
        config.load_private_key()
        public_key = config.public_key
        private_key = config.private_key
        
        res = EtherUtil.send_ether(public_key, private_key, to, amount, units)
        val = EtherUtil.wait_for_transaction(res)
        return val

    @classmethod
    def help_send_rbtc(cls):
        print("Transfers the amount of rBTC from your address to the target address.")
        print("Example:\n bapc.sh rbtc send <target_address> amount units")
        print(" bapc.sh rbtc send 0x50D7b0aDfd593B8972d6bDcE974E43d37653e431 0.5 ether")


    @classmethod
    def cash_out_rbtc(cls, *args, **kwargs):
        if len(args) < 2:
            raise ARGException("Not enough arguments.")
        amount = args[0]
        units = args[1]

        if float(amount) > 0:
            try:
                value = config.w3.toWei(amount, units)
            except:
                raise ARGException("Invalid unit type.  Must be 'ether', 'wei', gwei', etc.")
        else:
            raise ARGException("Amount to transfer from must be > 0")
        config.load_private_key()
        public_key = config.public_key
        private_key = config.private_key
        to = "0x0000000000000000000000000000000001000006"
        res = EtherUtil.send_ether(public_key, private_key, to, amount, units, gas_price=60000000, estimate_gas=100000)
        val = EtherUtil.wait_for_transaction(res)
        if val['status'] == 1:
            print("Cash out successful.")
        return val

    @classmethod
    def help_cash_out_rbtc(cls):
        print("Transfers the amount of rBTC to the bridge contract to convert into BTC.  The BTC will be sent to the BTC address corresponding to the rBTC private key.")
        print("Example:\n bapc.sh rbtc cashOut amount units")
        print(" bapc.sh rbtc cashOut 0.5 ether")

    @classmethod
    def convert_btc_to_rbtc_address(cls, *args):
        #if len(args) < 1:
        #    raise ARGException("Not enough arguments.")
        #btc_private_key = args[0]
        btc_private_key = input("BTC private key: ")

        if not btc_private_key.startswith("p2pkh:"):
            print("Key must start with \"p2pkh\"")
            return False
        key = btc_private_key[6:]
        res = base58.b58decode(key)
        private_key = "0x"+res[1:-5].hex()
        public_key = config.w3.eth.account.from_key(private_key).address
        print("Public Key:", public_key)
        print("Private Key:", private_key)
        return public_key, private_key

    @classmethod
    def help_convert_btc_to_rbtc_address(cls):
        print("Computes the corresponding rBTC address from the BTC private key (in p2pkh format).  Will print out the corresponding public and private keys for use with RSK.  To use this rBTC, whitelist the BTC address on the RSK gitter channel: https://gitter.im/rsksmart/rskj and then send BTC to the federation address. Use 'bapc.sh rbtc getFederationAddress' for more information.")
        print("Addresses on the testnet do not need to be whitelisted first.")
        print("Example:\n bapc.sh rbtc computeFromBTCKey")
        print(" BTC private key: p2pkh:cQAABBBCCCcG2cjYGaeZZZZtp0BSuDKA5SrQY0JXfo0vf0j0000L")


